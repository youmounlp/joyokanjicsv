## joyokanjicsv

秒で使える機械判読可能な常用漢字表（全2136字）

### 使い方

以下のURLより直接ダウンロードできます


```sh
https://gitlab.com/youmounlp/joyokanjicsv/-/raw/master/joyokanji.csv
```

### フォーマット

- 1列目　通し番号（1〜2136、五十音順）
- 2列目　漢字（1文字）
- 3列目　音読み（カタカナ）、複数ある場合はスラッシュ区切り、無い場合は空欄
- 4列目　訓読み（ひらがな）、複数ある場合はスラッシュ区切り、無い場合は空欄

### 作成手順

1. 参考資料にある文化庁のページより常用漢字表をコピペ
1. スプレッドシート等の表計算ソフトに貼り付け
1. 「漢字」列の1文字目を抽出、「音訓」列の改行をスラッシュに置換し、csv形式で保存（joyokanji_raw.csv）
1. Pythonスクリプト（process.ipynb）で音読み・訓読みを判別し、別の列に入るよう整形

### ライセンス

パブリック・ドメイン

### 参考資料

[文部科学省ウェブサイト利用規約](https://www.mext.go.jp/b_menu/1351168.htm)に従い、[文化庁 | 国語施策・日本語教育 | 国語施策情報 | 常用漢字表の音訓索引](https://www.bunka.go.jp/kokugo_nihongo/sisaku/joho/joho/kijun/naikaku/kanji/joyokanjisakuin/index.html)をもとに作成